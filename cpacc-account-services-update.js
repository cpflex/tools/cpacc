#! /user/bin/env node
/*
 *  Name:  cacc-account-services-updates.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");

var fs = require("fs");
try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description("Updates one or more services for the specified account.")
    .option(
      "-s, --spec <specfile>",
      "Service(s) specification file can be JSON (.json) or YAML (.yaml,.yml)."
    )
    .arguments(
      "<acct>",
      "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key."
    )
    .arguments("[servicespec]", "Optional service specification.")
    .action(function (acct, servicespec) {
      _acct = acct;
      _servicespec = servicespec;
    });
  acctools.addCommonOptions(program);
  program.parse(process.argv);

  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------
  //Load input spec if defined.
  var specs = [];

  //Add command line spec if defined.
  if (Array.isArray(_servicespec)) specs = specs.concat(_servicespec);
  else if (_servicespec != null) specs.push(_servicespec);

  //Merge input file if defined.
  if (!(program.spec === undefined)) {
    var obj = acctools.loadSpecification(program.spec);
    if (Array.isArray(obj)) specs = specs.concat(obj);
    else specs.push(obj);
  }

  var inputspec = {
    acct: _acct,
    services: specs,
  };

  if (program.verbose) {
    console.log("Adding  services:");
    console.log(util.inspect(inputspec, false, null, true));
  }

  var req = acctools.postApiRequest(
    program,
    "/account/services/update",
    inputspec
  );

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    acctools.publishOutput(program, resp);
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
