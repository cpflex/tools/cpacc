#! /user/bin/env node
/*
 *  Name:  cpacc-device-status.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./benttools");
const { program } = require("commander");
var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description("Reports the status of the devices specified.", {
      devids: "Optional one or more devids.",
    })
    .option(
      "-a, --acct <acct>",
      "Optional acct path or UUID, which reports the status of the devices in the account. "
    )
    .option("-d, --detailed", "Show detailed status", false)
    .option(
      "-s, --specfile <filename>",
      "File containing an array of one or more devids in json or yaml format."
    )

    .arguments("[devids...]")
    .action(async function (devids) {
      //------------------------------------------------------------------------
      // Command Implementation
      //------------------------------------------------------------------------
      var ct = 0;

      var mapAcct = new Map();
      var devices = [];
      var report = [];
      var data = [];
      const result = acctools.createPostClient(program, false, [200]);
      const postApi = result.post;
      const hdr = result.hdr;

      //Get the list of devices, either from files or account
      if (program.acct) {
        const acct = await getAcct(program.acct, postApi, hdr);
        mapAcct.set(acct.acctid, acct);
        devices = await getDeviceList(acct.acctid, postApi, hdr);
      }

      //Load the specfile if defined.
      if (program.specfile) {
        const devNew = acctools.loadSpecification(program.specfile);
        if (!Array.isArray(devNew)) {
          devices.push(devNew);
        } else {
          devices = devices.concat(devNew);
        }
      }

      //Add any device ids that are defined.
      if (devids) {
        if (Array.isArray(devids)) {
          devices = devices.concat(devids);
        } else {
          devices.push(devids);
        }
      }

      //Get all the devices (todo make scalable)
      if (program.verbose) {
        console.info(`Retrieving device status for ${devids.length} device(s)`);
      }

      for (devid of devices) {
        try {
          const rsDevice = await getDevice(devid, postApi, hdr);
          if (!mapAcct.get(rsDevice.device.acctid)) {
            mapAcct.set(rsDevice.device.acctid, null);
          }

          data.push(rsDevice);
        } catch (eget) {
          if (eget.statusCode) {
            console.error(
              `Error retrieving device ${ct}: devid=${devid}, message = ${
                eget.message
              }(${eget.statusCode}), text=${await eget.text()}`
            );
          } else {
            console.error(
              `Error retrieving item ${ct}: devid=${devid}, message = ${eget.message}`
            );
          }
        }
        ct++;
      }

      //Get all the accounts
      if (program.verbose) {
        console.info(
          `Retrieving account information for ${mapAcct.length} account(s)`
        );
      }

      for (const [key, val] of mapAcct) {
        if (val == null) {
          try {
            const rsacct = await getAcct(key, postApi, hdr);
            mapAcct.set(key, rsacct);
          } catch (eget) {
            if (eget.statusCode) {
              console.error(
                `Error retrieving account ${ct}: acctid=${acctid}, message = ${
                  eget.message
                }(${eget.statusCode}), text=${await eget.text()}`
              );
            } else {
              console.error(
                `Error retrieving device ${ct}: acctid=${devid}, message = ${eget.message}`
              );
            }
          }
        }
      }

      var bHasDeviceSub = false;
      data.forEach((value) => {
        bHasDeviceSub |= value.stats.plantype != "account";
      });

      const results = formatResults(
        data,
        mapAcct,
        bHasDeviceSub,
        program.detailed
      );
      console.info(`** Processed ${ct} devices`);
      acctools.publishOutput(program, results);
    });

  acctools.addCommonOptions(program, true);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}

/**
 * Outputs the results
 * @param {[{device:{}, stats:{}}]} devices Array of device data
 * @param {Map<acctid,{}} mapAcct Account map
 * @param {boolean} bHasDeviceSub If true, device has a subscription.
 * @param {boolean} bDetailed If true, detailed results are generated.
 */
function formatResults(devices, mapAcct, bHasDeviceSub, bDetailed) {
  const result = [];
  devices.forEach((d) => {
    const dev = d.device;
    const stat = d.stats;
    let rpt = {
      devid: dev.devid,
      serial: dev.serialnumber,
      state: dev.state,
      network: dev.network,
      lastactivity: stat.lastactivity,
      lifetimeCredits: stat.ctlifetime,
      devicetype: dev.devicetype,
      productid: dev.productid,
    };

    if (bDetailed) {
      const act = mapAcct.get(dev.acctid);
      Object.assign(rpt, {
        devlabel: dev.label,
        acctid: dev.acctid,
        acctpath: act.path,
        acctlabel: act.label,
        created: dev.created,
        updated: dev.updated,
        app: dev.appuri,
        firmware: dev.firmwareuri,
        api: dev.apiuri,
      });

      if (bHasDeviceSub) {
        Object.assign(rpt, {
          plantype: stat.plantype,
          creditsRemaining: stat.credits,
          enrollstart: stat.enrollstart,
          enrollstop: stat.enrollstop,
        });
      }
    }
    result.push(rpt);
  });
  return result;
}

/**
 * Gets device status and details.
 * @param {*} devid
 * @param {*} postApi
 * @param {*} hdr
 * @returns {promise<{details:{*}, status:{*}}>}
 */
async function getDevice(devid, postApi, hdr) {
  const inputspec = {
    devid: devid,
  };
  return postApi("/device/info", inputspec, hdr);
}

/**
 * Gets list of devices for an account.
 * @param {*} devid
 * @param {*} postApi
 * @param {*} hdr
 * @returns {promise<[string]>} Returns array of devid strings.
 */
async function getDeviceList(acct, postApi, hdr) {
  const inputspec = {
    acct: acct,
    incdeleted: false,
    verbose: false,
  };
  return postApi("/device/list", inputspec, hdr).then((result) => {
    return result.map((d) => d.devid);
  });
}

/**
 * Gets the account information
 * @param {*} acct
 * @param {*} postApi
 * @param {*} hdr
 * @returns {promise<object>} Returns the account information.
 */
async function getAcct(acct, postApi, hdr) {
  const inputspec = {
    acct: acct,
    incdeleted: false,
    fullpath: true,
  };
  return postApi("/account/info", inputspec, hdr);
}
