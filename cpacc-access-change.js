#! /user/bin/env node
/*
 *  Name:  cpacc-access-change.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");

var fs = require("fs");
try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description(
      "**REQUIRES ACCESS KEY**\r\n Changes ACL access key and method.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
        acckeynew: "New access key.",
        accmethod: 'Access key validation method: either "password" or "hmac".',
      }
    )
    .arguments("<acct> <acckeynew> <accmethod>")
    .action(function (acct, acckeynew, accmethod) {
      _acct = acct;
      _acckeynew = acckeynew;
      _accmethod = accmethod;
    });
  acctools.addCommonOptions(program, true);
  program.parse(process.argv);

  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------
  var inputspec = {
    acct: _acct,
    acckeynew: _acckeynew,
    accmethod: _accmethod,
  };

  //console.log( inputspec);
  var req = acctools.postApiRequest(
    program,
    "/security/access/keychange",
    inputspec,
    true
  );

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    acctools.publishOutput(program, resp);
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
