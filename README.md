# CP-Flex &trade; Account Provisioning Command Line Tools

Node JS based tools provide a easy-to-use command line interface for interacting
with the CP-Flex account provisioning and management APIs.  The APIs provide full access to the CP-Flex platform accounts, devices, subscription plans/keys, and access control.   If you do not have an account with an assigned API key, please contact Codepoint personnel.


#Installation

## Install Node.js  

The tools require Node.js (javascript) tools to install your system.   If not already installed, please find the package for your system an install it. Go to https://nodejs.org/en/download/ and select your OS.  Follow the instructions there.

## Clone and Install cpacc.
The tools here are essentially scripts that can be packaged and deployed globally on your system. This provides a command line interface for working with CP-Flex provisioning APIs.   

1. Install Git.  The best way to obtain the tools is to use GIT this can be found at https://git-scm.com/downloads 
1. Clone this project to your local computer.   Open a command window or terminal, change to a working directory, where you want to clone the source files (e.g. .../projects/cpflex/) and execute the following comand.  

    ```
    clone https://gitlab.com/cpflex/tools/cpacc.git
    ```

1. Once cloned, change directory into the cpacc project and install the the package locally

    ```
    npm install
    ```
  
  For developer's, the source code is freely available to see how to use the APIs.   

1. To access the tools from anywhere on your machine, package them as a single executable.  Install pkg to create a single binary and then use the tool to create executables for Windows, MacOS, or linux.

    ```
    npm install -g pkg
    pkg .
    ```
  This will create cpacc-win.exe, cpacc-linux, and cpacc-macos.


1. Select the package for you OS and copy it to a folder that is on your path in order to execute globally.
  For example, copy the *cpacc-win.exe* to a tools folder (e.g. `C:/bin`) and rename the file, *cpacc.exe*.


1. Once installed, change your working directory to a project folder and execute the following command:

    ``` 
    cpacc 
    ```

1. If successful, it will return the following information.

    ```
    Usage: cpacc [options] [command]

    CP-Flex account management command tools. Look at the "--help" option for each of the commands to learn more.

    Options:
      -V, --version               output the version number
      -U, --url <url>             Overrides default or "CPACC_URL" or "CPFLEX_DOMAIN" (global.cpflex.tech) environment variable.      (default: "http://global.cpflex.tech/provisioning/v1.0")
      -J, --json                  formats output as json. Useful when capturing in a file. (default: false)
      -K, --apikey <apikey>       CP-Flex account API Key must be defined or specified in environment variable "CPFLEX_APIKEY"
      -v, --verbose               Verbose output. (default: false)
      -h, --help                  display help for command

    Commands:
      account [services|apikeys]  Command set for managing accounts
      device                      Command set for managing devices.
      subscription <plans|keys>   Command set for managing subscriptions.
      access                      Commands for managing access to controlled resources in CP-Flex.
      help [command]              display help for command
    ```

1.  Set up you environment variables with your account apikey and access key:  

  On Windows:
    set CPFLEX_APIKEY="<YOUR API KEY>"
    set CPFLEX_ACCKEY="<YOUR ACCKEY>" (optional)

  On Linux or similar systems
    declare -x CPFLEX_APIKEY='<YOUR API KEY>"
    declare -x CPFLEX_ACCKEY='<YOUR ACCKEY>" (optional)

  Depending on your access level, you may issued just an API KEY.   For trusted resellers and partners an additional access key (ACCKEY) may be provided that unlocks additional APIs using two factor authentication (either password or hmac). Should you need access to special capabilities of the CP-Flex platform contact your Codepoint support.    

  These environment variables can be made permanent by storing in your system configuration. You can also add these on the command line (the '-K' option) if you are changing API KEYS often or don't want them to persist.   

# Using the Provisioning Tools
*cpacc* provides a thin wrapper over the API's facilitating basic management operations.  All data used by the tools are provided either on the command line or by separate JSON files. The *template* folder included with the source code has the basic JSON structure for all essential provisioning data types. The best practice is to copy the template folder and then copy and modify the internal files as needed when adding or updating various provisioning data records.  

For example, lets create an account.   

1. Copy the `template/new_account.json` file to a file `account_data/my_first_account.json`  Open the file in an editor and populate all the fields:
    ```
    {
      "admincontact": {
        "name": "Sandy Blaster",
        "email": "sblaster@codepoint.xyz",
        "mobile": "+15553331212"
      },
      "attributes": null,
      "label": "My First Account",
      "description": "This is my first account to learn how to use the API.",
      "services" : [
        {
          "typeuri": "positioning",
          "name": "cps",
          "apikey": "<traxmate API KEY>",
          "attributes": null,
          "description": "My Traxmate account.",
          "url": "https://cps.combain.com/"
        }
      ]
    }
    ```
  Assuming you are going to use the indoor location features of the platform, you will need to obtain the API key for your Traxmate or combain account.  Be sure the value is specified in the apikey above.  If not specified, devices associated with this account will not be determin their location.

1. Execute the cpacc command to add an account.

    ```
    cpacc account add myaccount customer  -s account_data/my_first_account.json

    ```

  This will return.

    ```
    {
      apikey: 'tyWxg9XZ2ZE1GXF6SsOT34T4Wiv340JV',
      account: {
        acctid: 'b1c3f1d0-1e23-4b89-8b1d-e769624d9b19',
        accttype: 'customer',
        admincontact: {
          name: 'Sandy Blaster',
          email: 'sblaster@codepoint.xyz',
          mobile: '+15558881212'
        },
        attributes: null,
        created: '2020-11-07 06:10:16.807Z',
        description: 'This is my first account to learn how to use the API.',
        label: 'My First Account',
        path: '//resellers/<myreseller>/myaccount',
        state: 'active',
        termination: null
      }
    }
    ```


---
*Copyright &copy; 2020 Codepoint Technologies, Inc.*
