#! /user/bin/env node
/*
 *  Name:  cacc-account-apikeys-delete.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");
var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description(
      "**REQUIRES ACCESS KEY**\r\n Deletes the specified API key from the specified account.  The key is not recoverable if deleted.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
        apikey: "The API key to delete",
      }
    )
    .arguments("<acct> <apikey>")
    .action(function (acct, apikey) {
      _acct = acct;
      _apikey = apikey;
    });
  acctools.addCommonOptions(program, true);
  program.parse(process.argv);

  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------

  var specs = [];

  //Load input spec if defined.
  var inputspec = {
    acct: _acct,
    apikey: _apikey,
  };

  var req = acctools.postApiRequest(
    program,
    "/account/apikeys/delete",
    inputspec,
    true
  );

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    acctools.publishOutput(program, resp);
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
