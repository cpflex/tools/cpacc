#! /user/bin/env node
/*
 *  Name:  cpact-account-convert.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");

var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description(
      "**REQUIRES ACCESS KEY**\r\nSwitches the account and devices to the type of account specified by the registration key.   This is used to migrate devices from device-based accounts to account-based or vice versa.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",

        regkey:
          "Required registration key, which enrolls the devices or account when converted.  For device-based accounts, make sure the count of uses is sufficient to convert all the devices",
      }
    )
    .arguments("<acct> <regkey>")
    .action(function (acct, regkey) {
      //------------------------------------------------------------------------
      // Command Implementation
      //------------------------------------------------------------------------

      //Define the input using arguments and options.
      var inputspec = {
        acct: acct,
        key: regkey,
      };
      var req = acctools.postApiRequest(
        program,
        "/account/subscription/convert",
        inputspec,
        true
      );

      request(req, (error, response, body) => {
        var resp = acctools.parseResponseBody(error, response, body);
        acctools.publishOutput(program, resp);
      });
    });
  acctools.addCommonOptions(program);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
