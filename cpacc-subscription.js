#! /usr/bin/env node
/**
 *  Name:  cpacc-subscription.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

const acctools = require("./tools");
const { program } = require("commander");

program
  .description("CP-Flex suscription management tools")
  .version("1.0.0")
  .command("plans", "Command set for managing subscription plans.")
  .command("keys", "Command set for managing subscription keys.");

//acctools.addCommonOptions(program);
program.parse(process.argv);
