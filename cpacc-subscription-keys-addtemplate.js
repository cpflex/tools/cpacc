#! /user/bin/env node
/*
 *  Name:  cpacc-subscription-keys-addtemplate.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022-23 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./benttools");
const { program } = require("commander");

var fs = require("fs");
try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description(
      "**REQUIRES ACCESS KEY**\r\nCreates a new subscription key using the specified template identifier.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
        templateid:
          'The template identifier.  Can be a UUID or typed element URI with format [tclass]//[typeuri]:[name].  "tclass" is optional and will be assigned "template"',
      }
    )
    .arguments("<acct> <templateid>")
    .option(
      "-l, --label <label>",
      "Optional label specifier for the account, which a user friendly name for the account"
    )
    .option("-d, --descr <description>", "Optional key description.")
    .option(
      "-k, --count <number>",
      "Count of keys to generate using the current configuration. Default value is 1",
      1
    )
    .option(
      "-p, --params <key1=value1|key2=value2>",
      "Optional template parameter name value pairs. These are combined with any static parameters defined by the template and/or referenced lambdas.  Specify as '-p name1=value1|name2=value2|name3=value3 -- [other options or arguments].  Use quotes to ensure proper parsing."
    )

    //------------------------------------------------------------------------
    // Command Implementation
    //------------------------------------------------------------------------
    .action(async function (acct, templateid) {
      //Generate the API
      const result = acctools.createPostClient(program, true, [200]);
      const postApi = result.post;
      const hdr = result.hdr;

      var inputspec = {
        ...result.body,
        acct: acct,
        templateid: templateid,
      };

      //Construct parameters if defined.
      if (program.params != null) {
        const params = program.params.split("|");
        const parameters = {};
        for (const entry of params) {
          const result = entry.trim().split("=");
          if (result == null || result.length != 2) {
            throw new Error(
              "Parameter key value not valid (must be [key]=[value]): " + entry
            );
          }

          //Add the parameter.
          parameters[result[0]] = result[1];
        }

        inputspec.parameters = parameters;
      }

      if (program.label != null) {
        inputspec.label = program.label;
      }

      if (program.descr != null) {
        inputspec.description = program.descr;
      }

      //Generate the specified number of keys.
      const count = Number(program.count);
      var results = [];
      var idx = 0;
      for (; idx < count; idx++) {
        //update label with generation count if > 1.
        if (program.label != null && count > 1) {
          inputspec.label = `${program.label}-${idx + 1}`;
        }

        try {
          const rsKey = await postApi(
            "/subscription/keys/addtemplate",
            inputspec,
            hdr
          );
          results.push(rsKey);
        } catch (e2) {
          console.error(
            `JSON ERROR: \r\n${JSON.stringify(await e2.json(), null, 2)}`
          );
        }
      }

      //Publish the results.
      console.info(`** Processed ${idx} keys`);
      acctools.publishOutput(program, results);
    });

  acctools.addCommonOptions(program, true);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
