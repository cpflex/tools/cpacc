#! /user/bin/env node
/*
 *  Name:  cpacc-device-purge.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020,2023 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./benttools");
const { program } = require("commander");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description(
      "**REQUIRES ACCESS KEY**\r\nPurges one or more devices in an account. Will also delete device if option specified.",
      {
        acct:
          "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or " +
          "relative to the account associated with the API key.  The '.' can also be used to reference the " +
          "current account defined by the API key.",
        devids: "One or more device identifiers.",
      }
    )
    .arguments("[acct] [devids...]")
    .option("-a, --account", "purge all devices in the account.", false)
    .option(
      "-s, --spec <specfile>",
      "Device specification file can be JSON (.json) or YAML (.yaml,.yml). Has format {'acct':[acct], 'devids': " +
        ": [ '<devids>',...], 'delete': [true|false], 'account':[true|false], 'force':[true|false] }."
    )
    .option("-d, --delete", "First deletes the device then purges.", false)
    .option(
      "-y, --delay <seconds>",
      "Delay between deleting and/or purging devices.  This is a work-around for Senet registration.",
      0
    )
    .option(
      "-f, --force",
      "Forces purging of a device even if not deleted first.",
      false
    )

    //------------------------------------------------------------------------
    // Command Implementation
    //------------------------------------------------------------------------
    .action(async function (acct, devids) {
      var ct = 0;

      //Set up arguments using a spec file if specified.
      if (program.spec) {
        var spec = acctools.loadSpecification(program.spec);
        if (spec.acct) acct = spec.acct;
        if (spec.devids) devids = spec.devids;
        if (spec.delete) program.delete = spec.delete;
        if (spec.account) program.account = spec.account;
        if (spec.force) program.force = spec.force;
      }

      try {
        const { post, hdr, body } = acctools.createPostClient(
          program,
          true,
          200
        );

        //If account, replace devids with all devices.
        if (program.account) {
          const acctDevices = await post(
            "/device/list",
            {
              acct: acct,
              incdeleted: true,
              verbose: false,
            },
            hdr
          );
          devids = acctDevices.map((item) => item.devid);
        }

        //Validate arguments.
        if (!acct || !devids || !Array.isArray(devids)) {
          throw new Error(
            "invalid input arguments: acct or devids not properly defined."
          );
        }

        //**** Delete the devices if spec'd
        if (program.delete) {
          console.info("********************");
          console.info("* Deleting Devices");
          console.info("********************");
          for (const devid of devids) {
            if (ct > 0 && program.delay > 0) {
              await delay(program.delay * 1000);
            }

            ct++;
            console.info(`${ct}. deleting  ${devid}`);
            await post(
              "/device/delete",
              {
                acct: acct,
                acckey: body.acckey,
                devid: devid,
                incdeleted: false,
              },
              hdr
            );
          }
        }

        //**** Purge Selected Devices.
        console.info("********************");
        console.info("* Purging Devices");
        console.info("********************");

        ct = 0;
        for (const devid of devids) {
          if (program.delay > 0) {
            await delay(program.delay * 1000);
          }

          ct++;
          console.info(`${ct}. purging  ${devid}`);
          await post(
            "/device/purge",
            {
              acct: acct,
              acckey: body.acckey,
              devid: devid,
              force: program.force,
              incdeleted: false,
            },
            hdr
          );
        }
      } catch (e) {
        acctools.handleException(e);
      }
      console.info(`** Processed ${ct} devices`);
    });

  acctools.addCommonOptions(program, true);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}

function delay(time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}
