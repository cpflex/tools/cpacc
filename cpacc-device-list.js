#! /user/bin/env node
/*
 *  Name:  cpacc-account-info.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");

var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description("Lists the devices contained in an account.", {
      acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
    })
    .option(
      "-d, --detailed",
      "List the detailed records of the device and dump. These are the complete records for devices in an account, not summaries"
    )
    .option("-i, --incdel", "Will include deleted accounts as well.", false)
    .option(
      "-e  --excludestats",
      "Removes stats prior to displaying device information."
    )
    .arguments("<acct>", "account path or UUID")
    .action(function (acct) {
      _acct = acct;
    });
  acctools.addCommonOptions(program);
  program.parse(process.argv);

  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------
  //Load input spec if defined.
  var inputspec = {
    acct: _acct,
    incdeleted: program.incdel === true,
    verbose: program.verbose,
  };

  var req = acctools.postApiRequest(program, "/device/list", inputspec);

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    ProcessList(resp);
  });

  async function ProcessList(resp) {
    //console.log(util.inspect(resp, false, null, true));
    if (resp.length > 0 && program.detailed === true) {
      var devices = [];
      for (dev of resp) {
        devices.push(
          new Promise((resolve) => {
            req = acctools.postApiRequest(program, "/device/info", {
              devid: dev.devid,
            });
            request(req, (error, response, body) => {
              var device = acctools.parseResponseBody(error, response, body);
              resolve(device);
            });
          })
        );
      }

      try {
        devices = await Promise.all(devices);
      } catch (e) {
        console.error(e.message);
        process.exit(-1);
      }

      if (program.excludestats) {
        let dev2 = devices;
        devices = [];
        dev2.forEach((item) => devices.push(item.device));
      }
      acctools.publishOutput(program, devices);
    } else if (resp.length > 0) {
      acctools.publishOutput(program, resp);
    } else {
      console.log("No devices found.");
    }
  }
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
