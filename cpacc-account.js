#! /user/bin/env node
/*
 *  Name:  cpacc-account.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const { program } = require("commander");
const acctools = require("./tools");

program
  .version("0.5.0")
  .description("Commands for managing account information")
  .command("apikeys", "Commands for managing account apikeys")
  .command("services", "Commands for managing account service integration")
  .command(
    "add    <parent> <name> <acctype> [acctspec] ",
    "Add account given specification."
  )
  .command(
    "addtemplate    <acct> <templateid> [regkey]",
    "Add account using a template."
  )
  .command(
    "applykey    <acct> <key>",
    "Apply a subscription key to an account."
  )
  .command(
    "convert    <acct> <regkey>",
    "Converts subscription enrollment of device and accounts."
  )
  .command("delete <account> ", "Marks an account deleted.")
  .command(
    "info   <account> ",
    "Returns account information given account id or path."
  )
  .command(
    "list   <account> ",
    "Lists account children information given account id or path."
  )
  .command(
    "purge  <account> ",
    "**REQUIRES ACCESS KEY** Purges an account from the system ."
  )
  .command(
    "purgemulti  [acctfile...] ",
    "**REQUIRES ACCESS KEY** Purges one ore more accounts in specified files."
  )
  .command("status <acct>", "Gets the account subscription enrollment status.")
  .command(
    "update [acctspec] ",
    "Updates account given account specification."
  );

//acctools.addCommonOptions(program);
program.parse(process.argv);
