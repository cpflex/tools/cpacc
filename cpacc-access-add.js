#! /user/bin/env node
/*
 *  Name:  cpacc-access-add.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");

var fs = require("fs");
try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description(
      "**REQUIRES ACCESS KEY**\r\n Adds a resource access control record for an account.\r\n" +
        "The access record defines the access control and any account impersonations.  For example,\r\n\r\n" +
        "{  \r\n" +
        '  "acl" : [ {"uri" : "res://svcacc/...", "acc": 255}, ...] \r\n' +
        '  "impersonations" : [4caff89e-ffd2-4bc9-8d3f-0354a5f67e9b] \r\n' +
        "} \r\n\r\n" +
        "Access Flags: read(1), write(2), list(4), create(8), delete(16), purge(32), execute(64), all(127)\r\n" +
        "Add the access flag values together to determine the acc: e.g. acc= read(1) + write(2) = 3.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
        accrec: "Optional access record if not specified by file.",
      }
    )
    .option(
      "-s, --spec <specfile>",
      "Access record specification can be JSON (.json) or YAML (.yaml,.yml)."
    )
    .arguments("<acct> [accrec]")
    .action(function (acct, accrec) {
      _acct = acct;
      _accrec = accrec;
    });
  acctools.addCommonOptions(program, true);
  program.parse(process.argv);

  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------
  //Load input spec if defined.
  var rec = {};

  //Merge input file if defined.
  if (!(program.spec === undefined)) {
    acctools.mergeObjects(rec, acctools.loadSpecification(program.spec));
  }

  //Merge commandline if defined
  if (!(_accrec === undefined)) acctools.mergeObjects(rec, JSON.parse(_accrec));

  var inputspec = {
    acct: _acct,
    accrec: rec,
  };

  //console.log( inputspec);
  var req = acctools.postApiRequest(
    program,
    "/security/access/add",
    inputspec,
    true
  );

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    acctools.publishOutput(program, resp);
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
