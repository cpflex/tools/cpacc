#! /user/bin/env node
/*
 *  Name:  cpacc-account-info.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");

var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description(
      "Lists account children information given account id or path.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
      }
    )
    .option("-r, --recurse <depth>", "Recursion depth.", "1")
    .option("-i, --incdel", "Will include deleted accounts as well.", false)
    .option(
      "-p, --fullpath",
      "Shows fullpath if specified, relative path otherwise",
      false
    )
    .arguments("<acct>")
    .action(function (acct) {
      _acct = acct;
    });
  acctools.addCommonOptions(program);
  program.parse(process.argv);

  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------

  //Load input spec if defined.
  var inputspec = {
    acct: _acct,
    recurse: parseInt(program.recurse),
    fullpath: program.fullpath === true,
    incdeleted: program.incdel === true,
    verbose: program.verbose === true,
  };

  var req = acctools.postApiRequest(program, "/account/list", inputspec);

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    //console.log(util.inspect(resp, false, null, true));
    if (resp.length > 0) {
      acctools.publishOutput(program, resp);
    } else {
      console.log("No child accounts found.");
    }
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
