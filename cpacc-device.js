#! /user/bin/env node
/*
 *  Name:  cpact-device.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const { program } = require("commander");
const acctools = require("./tools");

program
  .description("Commands for managing devices associated with an account.")
  .version("0.5.0")
  .command("applykey    <acct> <key>", "Apply a subscription key to a device.")
  .command("activate <deviceid> ", "Activates a registered device.")
  .command(
    "attributes <deviceid> [attributes]",
    "Retrieves, updates or appends device attributes."
  )
  .command(
    "delete [acct] [devids...]",
    "**REQUIRES ACCESS KEY** Deletes one or more devices in an account."
  )
  .command("info <deviceid> ", "Returns device information.")
  .command("list <acct>", "Lists the devices in an account.")
  .command(
    "move <deviceid> <network>",
    "**REQUIRES ACCESS KEY** Moves the device to a new network."
  )
  .command(
    "purge [acct] [devids...]",
    "**REQUIRES ACCESS KEY** Purges one or more devices from an account."
  )
  .command(
    "register [acct] [templateid] [claimkeys...]",
    "Register one or more devices supporting claim keys, templates, and detailed specifications."
  )
  .command(
    "state  <deviceid> <state>",
    "Sets the device activity state: registered, activated, subscription_expired, disabled, or deleted"
  )
  .command(
    "status [options] [devids...]",
    "Provides status report of one or more device ids."
  )
  .command(
    "update <deviceid> [devicespec]",
    "updates a device given specification."
  );
//acctools.addCommonOptions(program);
program.parse(process.argv);
