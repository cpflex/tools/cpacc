#! /user/bin/env node
/*
 *  Name:  cpacc-template-list.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");
var fs = require("fs");
const yaml = require("js-yaml");
const { DEFAULT_SCHEMA } = require("js-yaml");
const { sign } = require("crypto");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description("Gets the list of templates available for the account.", {
      acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
    })
    .option(
      "-d, --detail",
      "true returns a detailed list, false returns a short summary",
      false
    )
    .arguments("<acct>")
    .action(function (acct) {
      //------------------------------------------------------------------------
      // Command Implementation
      //------------------------------------------------------------------------

      //Load input spec if defined.
      var inputspec = {
        acct: acct,
      };

      var req = acctools.postApiRequest(program, "/template/list", inputspec);

      request(req, (error, response, body) => {
        var resp = acctools.parseResponseBody(error, response, body);

        if (!program.detail) {
          const summary = [];
          for (const r of resp) {
            summary.push({
              elemid: r.elemid,
              elemuri: r.elemuri,
              acctid: r.acctid,
              label: r.label,
            });
          }
          acctools.publishOutput(program, summary);
        } else {
          acctools.publishOutput(program, resp);
        }
      });
    });
  acctools.addCommonOptions(program);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
