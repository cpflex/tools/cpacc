#! /user/bin/env node
/*
 *  Name:  cpact-account-addtemplate.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");

var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description(
      "Adds a account to the system given parent account, template specifier and optional subscription registration key (regkey).",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",

        templateid:
          'The typed element identifier.  Can be a UUID or typed element URI with format [tclass]//[typeuri]:[name].  Tclass is optional and will be assigned "template"',
        regkey:
          "Optional subscription registration key.  If provided, account wil manage the subscription as opposed to device-based subscription management.  Some templates may require this specification.",
      }
    )
    .arguments("<acct> <templateid> [regkey]")
    .option(
      "-n, --pathname <value>",
      "Optional unique account path name within the scope of the parent account. If not specified a name is automatically generated."
    )
    .option(
      "-l, --label <label>",
      "Optional label specifier for the account, which a user friendly name for the account"
    )
    .option("-d, --descr <description>", "Optional account description.")
    .option(
      "-p, --params <key1=value1|key2=value2>",
      "Optional template parameter name value pairs. These are combined with any static parameters defined by the template and/or referenced lambdas.  Specify as '-p name1=value1|name2=value2|name3=value3 -- [other options or arguments].  Use quotes to ensure proper parsing."
    )
    .action(function (acct, templateid, regkey) {
      //------------------------------------------------------------------------
      // Command Implementation
      //------------------------------------------------------------------------

      //Define the input using arguments and options.
      var inputspec = {
        acct: acct,
        templateid: templateid,
      };

      if (regkey != null) {
        inputspec.regkey = regkey;
      }

      if (program.pathname != null) {
        inputspec.name = program.pathname;
      }

      if (program.label != null) {
        inputspec.label = program.label;
      }

      if (program.descr != null) {
        inputspec.description = program.descr;
      }

      //Construct parameters if defined.
      if (program.params != null) {
        const params = program.params.split("|");
        const parameters = {};
        for (const entry of params) {
          const result = entry.trim().split("=");
          if (result == null || result.length != 2) {
            throw new Error(
              "Parameter key value not valid (must be [key]=[value]): " + entry
            );
          }

          //Add the parameter.
          parameters[result[0]] = result[1];
        }

        inputspec.parameters = parameters;
      }

      var req = acctools.postApiRequest(
        program,
        "/account/addtemplate",
        inputspec
      );

      request(req, (error, response, body) => {
        var resp = acctools.parseResponseBody(error, response, body);
        acctools.publishOutput(program, resp);
      });
    });
  acctools.addCommonOptions(program);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
