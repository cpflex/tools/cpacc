#! /user/bin/env node
/*
 *  Name:  cpact-account-add.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description("Updates account given obj account specification.", {
      acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
      acctspec: "Optional account updated details.",
    })
    .option(
      "-s, --spec <specfile>",
      "Account specification file can be JSON (.json) or YAML (.yaml,.yml)."
    )
    .arguments("<acct> [acctspec]")
    .action(function (acct, acctspec) {
      _acct = acct;
      _acctspec = acctspec;
    });
  acctools.addCommonOptions(program);
  program.parse(process.argv);

  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------

  //Load input spec if defined.
  var inputspec = {};

  //Merge input file if defined.
  if (!(program.spec === undefined)) {
    var obj = acctools.loadSpecification(program.spec);
    acctools.mergeObjects(inputspec, obj);
  }

  //Merge commandline if defined
  if (!(_acctspec === undefined)) {
    acctools.mergeObjects(inputspec, JSON.parse(_acctspec));
  }

  //Set required properties.
  inputspec.acct = _acct;

  var req = acctools.postApiRequest(program, "/account/update", inputspec);

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    acctools.publishOutput(program, resp);
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
