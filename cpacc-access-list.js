#! /user/bin/env node
/*
 *  Name:  cpacc-access-list.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");

var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description(
      "**REQUIRES ACCESS KEY**\r\n Returns the access control records defined within the scope of the specified account.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
      }
    )
    .arguments("<acct>")
    .action(function (acct) {
      _acct = acct;
    });
  acctools.addCommonOptions(program, true);
  program.parse(process.argv);

  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------
  var inputspec = {
    acct: _acct,
  };

  //console.log( inputspec);
  var req = acctools.postApiRequest(
    program,
    "/security/access/list",
    inputspec,
    true
  );

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    acctools.publishOutput(program, resp);
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
