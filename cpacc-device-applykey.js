#! /user/bin/env node
/*
 *  Name:  cpact-device-applykey.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");

var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description(
      "Apply a subscription key to a device updating the subscription enrollment.  Used to renew subscription or apply credits.",
      {
        devid: "The device identifier.",

        key: "Required subscription key. Can can be for renewal or adding credits.",
      }
    )
    .arguments("<devid> <key>")
    .action(function (devid, key) {
      //------------------------------------------------------------------------
      // Command Implementation
      //------------------------------------------------------------------------

      //Define the input using arguments and options.
      var inputspec = {
        devid: devid,
        key: key,
      };
      var req = acctools.postApiRequest(
        program,
        "/device/subscription/applykey",
        inputspec
      );

      request(req, (error, response, body) => {
        var resp = acctools.parseResponseBody(error, response, body);
        acctools.publishOutput(program, resp);
      });
    });
  acctools.addCommonOptions(program);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
