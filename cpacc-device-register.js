#! /user/bin/env node
/*
 *  Name:  cpacc-device-register.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const acctools = require("./benttools");
const { program } = require("commander");
var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description(
      "Registers a new device with an account given a valid subscription key.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
        templateid:
          'The template identifier.  Can be a UUID or typed element URI with format [tclass]//[typeuri]:[name].  "tclass" is optional and will be assigned "template"',
        claimkeys:
          "zero or more claim keys.  If not specified a specfile must be provided.",
      }
    )
    .arguments("[acct] [templateid] [claimkeys...]")
    .option(
      "-s, --spec <specfile>",
      "Device specification file can be JSON (.json) or YAML (.yaml,.yml). Has format {'acct':[acct], 'templateid': " +
        "[template id], 'claimkeys': [ <claimkey>,...], 'regkey':[regkey], 'params:{*}, 'label': [label] " +
        "'delay':[delay], 'specOverride': {...}, 'deviceSpec':[{...}, ...] }" +
        " where 'specOverride' are the optional device specification properties (overrides template) applied to all claimkeys and 'deviceSpec'.  'deviceSpec' is manual specification of one or more devices."
    )
    .option(
      "-r, --regkey <regkey>",
      "Optional subscription registration key required for device-based subscription plans. Do not provide if account-based plans are in use for the account."
    )
    .option(
      "-p, --params <key1=value1|key2=value2>",
      "Optional template parameter name value pairs. These are combined with any static parameters defined by the template and/or referenced lambdas.  Specify as '-p name1=value1|name2=value2|name3=value3 -- [other options or arguments].  Use quotes to ensure proper parsing."
    )
    .option(
      "-l, --label <label>",
      "Optional label specifier for the device, which a user friendly name for the device.  In multiple device registration, a number will be appended to the label"
    )
    .option(
      "-y, --delay <seconds>",
      "Delay between registrations of multiple claim keys.  This is a work-around for Senet registration.",
      0
    )

    //------------------------------------------------------------------------
    // Command Implementation
    //------------------------------------------------------------------------
    .action(async function (acct, templateid, claimkeys) {
      var specOverride = {};
      var deviceSpecs = [];

      //Set up arguments using a specfile if specified.
      if (program.spec) {
        var spec = acctools.loadSpecification(program.spec);
        if (spec.acct) acct = spec.acct;
        if (spec.templateid) templateid = spec.templateid;
        if (spec.claimkeys) claimkeys = spec.claimkeys;
        if (spec.regkey) program.regkey = spec.regkey;
        if (spec.params) program.params = spec.params;
        if (spec.label) program.label = spec.label;
        if (spec.delay) program.delay = spec.delay;
        if (spec.specOverride) specOverride = spec.specOverride;
        if (spec.deviceSpec) {
          if (Array.isArray(spec.deviceSpec)) {
            deviceSpecs = spec.deviceSpec;
          } else {
            deviceSpecs = [spec.deviceSpec];
          }
        }
      }

      //Load input spec if defined.
      var inputspec = {
        acct: acct,
        templateid: templateid,
      };

      //Merge regkey option if defined
      if (!(program.regkey === undefined)) {
        inputspec.regkey = program.regkey;
      }

      addParams(inputspec, program);

      var ct = 0;
      console.info("Registering: ");
      try {
        let { post, hdr } = acctools.createPostClient(program, false, 200);

        //First Register all devices with Claimkeys.
        for (const claimkey of claimkeys) {
          if (ct > 0 && program.delay > 0) {
            await delay(program.delay * 1000);
          }

          //Merge all properties to for body.
          var body = {
            ...inputspec,
            ...specOverride,
            claimkey: claimkey,
          };

          ct++;
          console.info(`${ct}. ${claimkey}`);

          //Process Options.
          if (program.label != null) {
            body.label =
              program.label + (claimkeys.length > 1 ? ct.toString() : "");
          }
          await post("/device/register", body, hdr);
        }

        //Second Register all devices with deviceSpecs.
        for (const device of deviceSpecs) {
          if (ct > 0 && program.delay > 0) {
            await delay(program.delay * 1000);
          }

          //Merge all properties to for body.
          var body = {
            ...inputspec,
            ...device,
            ...specOverride,
          };

          ct++;
          console.info(`${ct}. ${device.devid}`);

          //Process Options.
          if (program.label != null) {
            body.label =
              program.label + (claimkeys.length > 1 ? ct.toString() : "");
          }
          await post("/device/register", body, hdr);
        }
      } catch (e) {
        acctools.handleException(e);
      }
    });

  acctools.addCommonOptions(program);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}

/**
 * Adds argument parameters if defined.
 * @param {*} inputspec
 * @param {*} program
 */
function addParams(inputspec, program) {
  //Construct parameters if defined.
  if (program.params != null) {
    if (typeof program.params == "string") {
      const params = program.params.split("|");
      const parameters = {};
      for (const entry of params) {
        const result = entry.trim().split("=");
        if (result == null || result.length != 2) {
          throw new Error(
            "Parameter key value not valid (must be [key]=[value]): " + entry
          );
        }
        parameters[result[0]] = result[1];
      }
      inputspec.parameters = parameters;
    } else {
      inputspec.parameters = program.params;
    }
  }
}

function delay(time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}
