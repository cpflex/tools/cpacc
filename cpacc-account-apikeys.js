#! /user/bin/env node
/*
 *  Name:  cpact-apikeys.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const { program } = require("commander");
const acctools = require("./tools");

program
  .version("1.0.0")
  .description("Commands for managing an account's API keys.")
  .command(
    "add <acct> [servicespec]",
    "**REQUIRES ACCESS KEY** Adds an API key."
  )
  .command(
    "delete <acct> <typeuri> <name>",
    "**REQUIRES ACCESS KEY** Deletes an API key."
  )
  .command("list <acct>", "Lists an account's API keys.");

//acctools.addCommonOptions(program);
program.parse(process.argv);
