#! /user/bin/env node
/*
 *  Name:  cpacc-device-move.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");
var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description(
      "**REQUIRES ACCESS KEY**\r\nMoves device to a new network provider.",
      {
        devid: "The device identifier to retrieve detailed information.",
        network:
          'The name of the new network provider to move the device.  Must be "" or the name of the service integration.',
      }
    )
    .arguments("<devid> <network>")
    .action(function (devid, network) {
      _devid = devid;
      _network = network;
    });
  acctools.addCommonOptions(program, true);
  program.parse(process.argv);

  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------

  //Load input spec if defined.
  var inputspec = {
    devid: _devid,
    newNetwork: _network,
  };

  var req = acctools.postApiRequest(
    program,
    "/device/change-network",
    inputspec,
    true
  );

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    acctools.publishOutput(program, resp);
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
