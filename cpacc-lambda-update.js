#! /user/bin/env node
/*
 *  Name:  cpacc-lambda-update.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");
const yaml = require("js-yaml");

var fs = require("fs");
var path = require("path");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description(
      "**REQUIRES ACCESS KEY**\r\nUpdates one or more lambdas given the specified lambda file specification supporting JSON and YAML formats.  The lambda update in YAML format is " +
        "typically simpler than using JSON. JSON is good for reading, transfer and copy.",
      {
        acct:
          "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  " +
          "The '.' can also be used to reference the current account defined by the API key.",
        lambdaFile:
          "one ore more lambda files containing the lambda information to load into the database.   Can be JSON or YAML (*.YML | *.YAML) file formats.  See lambda update examples for specifying these files.",
      }
    )
    .option(
      "-nc, --no-convert",
      "disables auto-conversion of the source package."
    )
    .arguments("<acct> [lambdaFile...]")
    .action(function (acct, lambdaFile) {
      //------------------------------------------------------------------------
      // Command Implementation
      //------------------------------------------------------------------------

      for (const fname of lambdaFile) {
        var lambdas = acctools.loadSpecification(fname);
        if (!Array.isArray(lambdas)) {
          lambdas = [lambdas];
        }

        // Remove the any whites space from the base64 encoded script.
        for (const lm of lambdas) {
          if (lm.sourcepkg != null) {
            if (program.convert) {
              //Encode string as base 64.
              lm.sourcepkg = Buffer.from(lm.sourcepkg, "utf-8").toString(
                "base64"
              );
            } else {
              //Assume base 64 encoding and remove any line-feeds or carriage returns.
              lm.sourcepkg = lm.sourcepkg.replace("\r").replace("\n").trim();
            }
          }
        }

        if (program.verbose) {
          console.info("Updating lambda definitions: " + fname);
          console.info(JSON.stringify(lambdas, null, 2));
        }

        var inputspec = {
          acct: acct,
          lambdas: lambdas,
        };

        var req = acctools.postApiRequest(
          program,
          "/lambda/update",
          inputspec,
          true
        );

        request(req, (error, response, body) => {
          var resp = acctools.parseResponseBody(error, response, body);
          acctools.publishOutput(program, resp);
        });
      }
    });

  acctools.addCommonOptions(program, true);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
