#! /user/bin/env node
/*
 *  Name:  cpacc-lambda-info.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program, option } = require("commander");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description(
      "**REQUIRES ACCESS KEY**\r\nGets one or more lambdas given the optional identifiers. If no identifiers specified, all detailed lambdas are returned.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
        identifiers: "Zero or more lambda identifiers to retrieve.",
      }
    )
    .option(
      "-nc, --no-convert",
      "disables auto-conversion of the source package from base64 to string."
    )
    .arguments("<acct> [identifiers...]")
    .action(function (acct, identifiers) {
      //------------------------------------------------------------------------
      // Command Implementation
      //------------------------------------------------------------------------

      if (identifiers != null && !Array.isArray(identifiers)) {
        identifiers = [identifiers];
      }

      if (identifiers.length == 0) {
        identifiers = null;
      }

      //Load input spec if defined.
      var inputspec = {
        acct: acct,
        lambdas: identifiers,
      };

      var req = acctools.postApiRequest(
        program,
        "/lambda/info",
        inputspec,
        true
      );

      request(req, (error, response, body) => {
        var resp = acctools.parseResponseBody(error, response, body);

        //Convert source package to UTF-8 if conversion enabled.
        if (program.convert) {
          if (resp != null && Array.isArray(resp)) {
            for (const lm of resp) {
              if (lm.sourcepkg != null) {
                lm.sourcepkg = Buffer.from(lm.sourcepkg, "base64").toString(
                  "utf-8"
                );
              }
            }
          }
        }

        acctools.publishOutput(program, resp);
      });
    });

  acctools.addCommonOptions(program);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
