#! /user/bin/env node
/*
 *  Name:  cpacc-template-info.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program, option } = require("commander");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description(
      "Gets one or more templates given the optional identifiers. If no identifiers specified, all detailed templates are returned.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
        identifiers: "Zero or more template identifiers to retrieve.",
      }
    )
    .arguments("<acct> [identifiers...]")
    .action(function (acct, identifiers) {
      //------------------------------------------------------------------------
      // Command Implementation
      //------------------------------------------------------------------------

      if (identifiers != null && !Array.isArray(identifiers)) {
        identifiers = [identifiers];
      }

      if (identifiers.length == 0) {
        identifiers = null;
      }

      //Load input spec if defined.
      var inputspec = {
        acct: acct,
        templates: identifiers,
      };

      var req = acctools.postApiRequest(program, "/template/info", inputspec);

      request(req, (error, response, body) => {
        var resp = acctools.parseResponseBody(error, response, body);
        acctools.publishOutput(program, resp);
      });
    });

  acctools.addCommonOptions(program);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
