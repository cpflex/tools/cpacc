#! /user/bin/env node
/*
 *  Name:  cpacc-template.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const acctools = require("./tools");
const { program } = require("commander");

var fs = require("fs");

program
  .version("0.5.0")
  .command(
    "add <acct> [templateFiles...]>",
    "**REQUIRES ACCESS KEY** Adds one or more templates for a specific an account."
  )
  .command(
    "update <acct> [templateFiles...]",
    "**REQUIRES ACCESS KEY** Updates one or more existing templates for a specific account."
  )
  .command(
    "info <acct> [identifiers...]",
    "Gets one or more templates given the optional identifiers."
  )
  .command("list <acct>", "Gets list of templates for an account.")
  .command(
    "purge <acct> [identifiers...]",
    "**REQUIRES ACCESS KEY** Purges specified templates from the system."
  );

//acctools.addCommonOptions(program);
program.parse(process.argv);
