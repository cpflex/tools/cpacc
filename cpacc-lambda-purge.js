#! /user/bin/env node
/*
 *  Name:  cpacc-lambda-purge.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");
var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description(
      "**REQUIRES ACCESS KEY**\r\nPurges the specified lambdas from the database.  Use carefully since other elements may depend on it.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
        lambdas: "One or more lambda identifiers to purge from the system.",
      }
    )
    .arguments("<acct> [lambdas...]")
    .action(function (acct, lambdas) {
      //------------------------------------------------------------------------
      // Command Implementation
      //------------------------------------------------------------------------

      //Load input spec if defined.
      var inputspec = {
        acct: acct,
        lambdas: lambdas,
      };

      var req = acctools.postApiRequest(
        program,
        "/lambda/purge",
        inputspec,
        true
      );

      request(req, (error, response, body) => {
        var resp = acctools.parseResponseBody(error, response, body);
        acctools.publishOutput(program, resp);
      });
    });

  acctools.addCommonOptions(program, true);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
