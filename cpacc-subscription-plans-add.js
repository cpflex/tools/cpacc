#! /user/bin/env node
/*
 *  Name:  cpacc-subscription-plans-add.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");

var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description("**REQUIRES ACCESS KEY**\r\nAdds a new subscription plan.", {
      acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
      planspec:
        "Optional subscription plan specification, see new_subscription.json template for example.",
    })
    .option(
      "-s, --spec <specfile>",
      "Subscription plan specification file can be JSON (.json) or YAML (.yaml,.yml)."
    )
    .arguments("<acct> [planspec]")
    .action(function (acct, planspec) {
      _acct = acct;
      _planspec = planspec;
    });
  acctools.addCommonOptions(program, true);
  program.parse(process.argv);

  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------

  //Load input spec if defined.
  var plan = {};

  //Merge input file if defined.
  if (!(program.spec === undefined)) {
    var obj = acctools.loadSpecification(program.spec);
    acctools.mergeObjects(plan, obj);
  }

  //Merge commandline if defined
  if (!(_planspec === undefined))
    acctools.mergeObjects(plan, JSON.parse(_planspec));

  var inputspec = {
    acct: _acct,
    plan: plan,
  };

  var req = acctools.postApiRequest(
    program,
    "/subscription/plans/add",
    inputspec,
    true
  );

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    acctools.publishOutput(program, resp);
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
