#! /user/bin/env node
/*
 *  Name:  cpacc-subscription-keys.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const acctools = require("./tools");
const { program } = require("commander");
require("console.table");
var fs = require("fs");

program
  .version("0.5.0")
  .command(
    "add <acct> <planid> <ctDevices>",
    "**REQUIRES ACCESS KEY** Add a new subscription key."
  )
  .command(
    "addtemplate <acct> <templateid>",
    "**REQUIRES ACCESS KEY** Add a new subscription key given the template identifier."
  )
  .command("info <acct> <key> ", "Gets subscription key information.")
  .command("list <acct>", "list subscription keys for an account.")
  .command(
    "purge <acct> <key> ",
    "**REQUIRES ACCESS KEY** Purges subscription key from database (use rarely)."
  );

//acctools.addCommonOptions(program);
program.parse(process.argv);
