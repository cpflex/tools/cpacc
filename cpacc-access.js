#! /user/bin/env node
/*
 *  Name:  cpacc-access.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const { program } = require("commander");
const acctools = require("./tools");

program
  .version("0.5.0")
  .description("CP-Flex Resource access control management.")
  .command(
    "add    <acct> <key> <method> [spec]",
    "Adds a new access control record."
  )
  .command(
    "change <acct> <key> <method>",
    "Changes an access control record access key."
  )
  .command("update <acct> [spec]", "updates an access control record.")
  .command("info   <acct> ", "Gets access control record.")
  .command("list", "list access control records for an account.")
  .command(
    "purge <acct> ",
    "Removes access control record from account and deletes from database."
  );

//acctools.addCommonOptions(program);
program.parse(process.argv);
