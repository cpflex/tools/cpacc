#! /user/bin/env node
/*
 *  Name:  cpacc-account-purgemulti.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./benttools");
const { program } = require("commander");
var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description(
      "**REQUIRES ACCESS KEY**\r\nPurges one or more accounts. Will also delete account if option specified.",
      {
        acctFile:
          "File containing an array of one or more account identifiers (either UUID or path) in json or yaml format.",
      }
    )
    .option("-d, --delete", "First deletes the device then purges.", false)
    .arguments("[acctFile...]")
    .action(async function (acctFile) {
      //------------------------------------------------------------------------
      // Command Implementation
      //------------------------------------------------------------------------
      var ct = 0;
      for (const fname of acctFile) {
        var accts = acctools.loadSpecification(fname);
        if (!Array.isArray(accts)) {
          accts = [accts];
        }

        if (program.verbose) {
          console.info("Purging accounts in file: " + fname);
        }

        for (const acct of accts) {
          console.info(`${ct}. "Purging: ${acct}`);
          try {
            if (program.delete) {
              console.info("   - deleting");
              var delspec = {
                acct: acct,
              };

              await acctools.postApiRequest(
                program,
                "/account/delete",
                delspec,
                true,
                [200, 204, 404]
              );
            }
            console.info("   - purging");
            var purgespec = {
              acct: acct,
              force: false,
            };

            await acctools.postApiRequest(
              program,
              "/account/purge",
              purgespec,
              true,
              [200, 204, 404]
            );
          } catch (e) {
            acctools.handleException(e, false);
          }

          ct++;
        }
      }
      console.info(`** Processed ${ct} accounts`);
    });

  acctools.addCommonOptions(program, true);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
