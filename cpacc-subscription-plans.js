#! /user/bin/env node
/*
 *  Name:  cpacc-subscription-plans.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const { program } = require("commander");
const acctools = require("./tools");

program
  .description("Commands form managing subscriptions.")
  .version("1.0.0")
  .command(
    "add    <acct> [planspec] ",
    "**REQUIRES ACCESS KEY** Add a new subscription plan."
  )
  .command(
    "info   <acct> <planid>",
    "Return a subscription plan detailed information."
  )
  .command("list   <acct>", "Lists all subscription plans for an account.")
  .command(
    "purge  <acct> <planid>",
    "**REQUIRES ACCESS KEY** Purges deleted account from database."
  )
  .command(
    "delete <acct> <planid>",
    "**REQUIRES ACCESS KEY** Set the subscription plan state."
  );
//acctools.addCommonOptions(program);
program.parse(process.argv);
