#! /user/bin/env node
/*
 *  Name:  cpacc-subscription-keys-add.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020-23 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./benttools");
const { program } = require("commander");

var fs = require("fs");
try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description(
      "**REQUIRES ACCESS KEY**\r\nCreates a new subscription key with required account, count of registrations, and subscription plan identifier.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
        keytype:
          'Optional key type can be one of "registration","renewal", or "credits"',
        ctdevices:
          "Optional count of devices that can be registered with this key.",
        planid:
          "Optional subscription plan encoded identifier. if not specified, SKU must be specified to provide lookup (-s option).",
      }
    )
    .arguments("<acct> [keytype] [ctdevices] [planid]")
    .option(
      "-t, --template <id>",
      "Optional template identifier, which can be UUID or elemuri."
    )
    .option(
      "-p, --params <key1=value1|key2=value2>",
      "Optional template parameter name value pairs. These are combined with any static parameters defined by the template and/or referenced lambdas.  Specify as '-p name1=value1|name2=value2|name3=value3 -- [other options or arguments].  Use quotes to ensure proper parsing."
    )
    .option(
      "-c, --credits <count>",
      "Optional additional credits to be added to a device."
    )
    .option(
      "-s, --sku <sku>",
      "Optional SKU associated with this subscription key."
    )
    .option(
      "-l, --label <label>",
      "Optional label identifying the subscription key."
    )
    .option(
      "-d, --descr <description>",
      "Optional description of the subscription key."
    )
    .option(
      "-k, --count <number>",
      "Count of keys to generate using the current configuration. Default value is 1",
      1
    )
    .option(
      "-a, --active <days>",
      "Optional days tag will be active.  If specified, key will no longer be useable after the specified date."
    )
    .option(
      "-V, --vis <visibility>",
      'Optional visibility can be "public", "protected", or "private"'
    )
    .action(async function (acct, keytype, ctdevices, planid) {
      //------------------------------------------------------------------------
      // Command Implementation
      //------------------------------------------------------------------------
      //Generate the API.
      const result = acctools.createPostClient(program, true, [200]);
      const postApi = result.post;
      const hdr = result.hdr;

      //Load input spec if defined.
      var specs = [];
      var inputspec = {
        ...result.body,
        acct: acct,
        key: {},
      };

      //Set the optional Arguments.

      if (keytype != null) inputspec.key.type = keytype;
      if (ctdevices != null) inputspec.key.total = ctdevices;
      if (planid != null) inputspec.key.planid = planid;
      if (program.template != null) inputspec.templateid = program.template;
      if (program.credits != null) inputspec.key.credits = program.credits;
      if (program.sku != null) inputspec.key.sku = program.sku;
      if (program.label != null) inputspec.key.label = program.label;
      if (program.descr != null) inputspec.key.description = program.descr;
      if (program.vis) inputspec.key.visibility = program.vis;
      if (program.active != null) {
        var dtNow = new Date();
        var dtExpires = new Date(dtNow.getTime() + program.active * 86400000);
        inputspec.key.expiration = dtExpires;
      }

      //Parse the parameters if defined.
      if (program.params != null) {
        const params = program.params.split("|");
        const parameters = {};
        for (const entry of params) {
          const result = entry.trim().split("=");
          if (result == null || result.length != 2) {
            throw new Error(
              "Parameter key value not valid (must be [key]=[value]): " + entry
            );
          }

          //Add the parameter.
          parameters[result[0]] = result[1];
        }

        inputspec.parameters = parameters;
      }

      //Generate the specified number of keys.
      const count = Number(program.count);
      var results = [];
      var idx = 0;
      for (; idx < count; idx++) {
        //update label with generation count if > 1.
        if (program.label != null && count > 1) {
          inputspec.label = `${program.label}-${idx + 1}`;
        }

        try {
          const rsKey = await postApi("/subscription/keys/add", inputspec, hdr);
          results.push(rsKey);
        } catch (e2) {
          console.error(
            `JSON ERROR: \r\n${JSON.stringify(await e2.json(), null, 2)}`
          );
        }
      }

      //Publish the results.
      console.info(`** Processed ${idx} keys`);
      acctools.publishOutput(program, results);
    });

  acctools.addCommonOptions(program, true);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
