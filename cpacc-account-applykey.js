#! /user/bin/env node
/*
 *  Name:  cpact-account-applykey.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");

var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description(
      "Apply a subscription key to an account updating a subscription enrollment.  Use to renew subscription or apply credits.",
      {
        acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",

        key: "Required subscription key. Can can be for renewal or adding credits.",
      }
    )
    .arguments("<acct> <key>")
    .action(function (acct, key) {
      //------------------------------------------------------------------------
      // Command Implementation
      //------------------------------------------------------------------------

      //Define the input using arguments and options.
      var inputspec = {
        acct: acct,
        key: key,
      };
      var req = acctools.postApiRequest(
        program,
        "/account/subscription/applykey",
        inputspec
      );

      request(req, (error, response, body) => {
        var resp = acctools.parseResponseBody(error, response, body);
        acctools.publishOutput(program, resp);
      });
    });
  acctools.addCommonOptions(program);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
