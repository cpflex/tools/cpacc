#! /user/bin/env node
/*
 *  Name:  cpacc-subscription-plans-info.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");
var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description("Lists subscription plans available for the account.", {
      acct: "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key.",
      planid: "The subscription plan identifier",
    })
    .arguments("<acct> <planid>")
    .action(function (acct, planid) {
      _acct = acct;
      _planid = planid;
    });
  acctools.addCommonOptions(program);
  program.parse(process.argv);

  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------

  //Load input spec if defined.
  var inputspec = {
    acct: _acct,
    planid: _planid,
  };

  var req = acctools.postApiRequest(
    program,
    "/subscription/plans/info",
    inputspec
  );

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    acctools.publishOutput(program, resp);
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
