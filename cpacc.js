#! /usr/bin/env node
/**
 *  Name:  cpacc.h
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

const acctools = require("./tools");
const { program } = require("commander");

program
  .description(
    'CP-Flex account management command tools. Look at the "--help"' +
      " option for each of the commands to learn more."
  )
  .version("1.1.2")
  .command("account [services|apikeys]", "Command set for managing accounts")
  .command("device ", "Command set for managing devices.")
  .command("lambda ", "Command set for managing lambda definitions.")
  .command(
    "subscription <plans|keys>",
    "Command set for managing subscriptions."
  )
  .command("template ", "Command set for managing templates.")
  .command(
    "access",
    "Commands for managing access to controlled resources in CP-Flex."
  );

//acctools.addCommonOptions(program);
program.parse(process.argv);
