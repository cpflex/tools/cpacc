#! /user/bin/env node
/*
 *  Name:  cpacc-device-attributes.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");

var fs = require("fs");

//------------------------------------------------------------------------
// Command arguments definition and setup.
//------------------------------------------------------------------------
program
  .description("Modifies a device attributes.", {
    devid: "The device identifier",
    attributes: 'attribute key-values "{"key":"value"}"',
  })
  .option(
    "-s, --spec <specfile>",
    "Device attributes specification file can be JSON (.json) or YAML (.yaml,.yml)."
  )
  .option(
    "-a, --append",
    "Appends attribute information to existing attributes"
  )
  .option("-r, --replace", "Replaces existing attribute information ")
  .option("-i, --info", "Retrieves current attribute information.")
  .arguments("<devid> [attributes]")
  .action(function (devid, attributes) {
    _devid = devid;
    _attributes = attributes;
  });
acctools.addCommonOptions(program);
program.parse(process.argv);
try {
  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------

  //Load input spec if defined.
  var attributes = {};

  //Merge input file if defined.
  if (!(program.spec === undefined)) {
    var obj = acctools.loadSpecification(program.spec);
    acctools.mergeObjects(attributes, obj);
  }

  //Merge commandline if defined
  if (!(_attributes === undefined)) {
    acctools.mergeObjects(attributes, JSON.parse(_attributes));
  }

  //Load device informationa.
  var inputspec = {
    devid: _devid,
  };

  var reqInfo = acctools.postApiRequest(program, "/device/info", inputspec);
  request(reqInfo, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);

    //Display current attributes if specified.
    let bUpdate = false;

    //Show attributes if specified or append and replace not specified.
    if (
      program.info === true ||
      !(program.append === true || program.replace === true)
    ) {
      acctools.publishOutput(program, resp.device.attributes);
      return; //All done.
    } else if (program.append === true) {
      //Append the results to existing attributes.
      let merge = resp.device.attributes;
      acctools.mergeObjects(merge, attributes);
      attributes = merge;
    }

    //Perform the update.
    inputspec.attributes = attributes;
    var req = acctools.postApiRequest(program, "/device/update", inputspec);

    request(req, (error, response, body) => {
      var resp = acctools.parseResponseBody(error, response, body);
      acctools.publishOutput(program, resp);
    });
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
