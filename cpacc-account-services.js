#! /user/bin/env node
/*
 *  Name:  cpact-Service.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const { program } = require("commander");
const acctools = require("./tools");

program
  .version("1.0.0")
  .description("Commands for managing an accounts service integrations.")
  .command(
    "add <acct> [servicespec]",
    "Adds one or more service integrations for specified account."
  )
  .command(
    "info <acct> <typeuri> <name>",
    "Returns the details of a specific service instance."
  )
  .command("list <acct>", "Lists the services for an account.")
  .command(
    "purge <acct> <typeuri> <name>",
    "**REQUIRES ACCESS KEY** Purges a service from an account."
  )
  .command(
    "update <acct> [servicespec]",
    "updates one or more service integrations for the specified account."
  );
//acctools.addCommonOptions(program);
program.parse(process.argv);
