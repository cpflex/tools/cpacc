#! /user/bin/env node
/*
 *  Name:  cpacc-device-info.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");
var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description("Returns detailed device information.", {
      devid: "The device identifier to retrieve detailed information.",
    })
    .arguments("<devid>")
    .action(function (devid) {
      _devid = devid;
    });
  acctools.addCommonOptions(program);
  //console.log(util.inspect(process.argv, false, null, true));
  program.parse(process.argv);

  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------

  //Load input spec if defined.
  var inputspec = {
    devid: _devid,
  };

  var req = acctools.postApiRequest(program, "/device/info", inputspec);

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    acctools.publishOutput(program, resp);
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
