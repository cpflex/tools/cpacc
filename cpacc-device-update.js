#! /user/bin/env node
/*
 *  Name:  cpacc-device-update.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");

var fs = require("fs");

//------------------------------------------------------------------------
// Command arguments definition and setup.
//------------------------------------------------------------------------
program
  .description(
    "Updates device information given the device specification on the command line or in a file." +
      "\r\nFields that can be updated: label, description, devicetype, appuri, apiui, description, attributes.",
    {
      devid: "The device identifier",
      devicespec: "Optional device specification",
    }
  )
  .option(
    "-s, --spec <specfile>",
    "Device specification file can be JSON (.json) or YAML (.yaml,.yml)."
  )
  .arguments("<devid> [devicespec]")
  .action(function (devid, devicespec) {
    _devid = devid;
    _devspec = devicespec;
  });
acctools.addCommonOptions(program);
program.parse(process.argv);

try {
  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------

  //Load input spec if defined.
  var inputspec = {};

  //Merge input file if defined.
  if (!(program.spec === undefined)) {
    var obj = acctools.loadSpecification(program.spec);
    acctools.mergeObjects(inputspec, obj);
  }

  //Merge commandline if defined
  if (!(_devspec === undefined)) {
    acctools.mergeObjects(inputspec, JSON.parse(_devspec));
  }

  inputspec.devid = _devid;

  var req = acctools.postApiRequest(program, "/device/update", inputspec);

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    acctools.publishOutput(program, resp);
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
