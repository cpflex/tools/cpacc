#! /user/bin/env node
/*
 *  Name:  cpacc-lambda.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2022 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
const acctools = require("./tools");
const { program } = require("commander");

var fs = require("fs");

program
  .version("0.5.0")
  .command(
    "add <acct> [lambdaFiles...]>",
    "**REQUIRES ACCESS KEY** Adds one or more lambdas for a specific an account."
  )
  .command(
    "update <acct> [lambdaFiles...]",
    "**REQUIRES ACCESS KEY** Updates one or more existing lambdas for a specific account."
  )
  .command(
    "info <acct> [identifiers...]",
    "**REQUIRES ACCESS KEY** Gets one or more lambdas given the optional identifiers."
  )
  .command("list <acct>", "Gets list of lambdas for an account.")
  .command(
    "purge <acct> [identifiers...]",
    "**REQUIRES ACCESS KEY** Purges specified lambdas from the system."
  );

//acctools.addCommonOptions(program);
program.parse(process.argv);
