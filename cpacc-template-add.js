#! /user/bin/env node
/*
 *  Name:  cpacc-template-add.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");
const yaml = require("js-yaml");

var fs = require("fs");
var path = require("path");
try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description(
      "**REQUIRES ACCESS KEY**\r\nCreates one or more templates given the specified template file specification supporting JSON and YAML formats.  Originating the template in YAML is " +
        "typically simpler than using JSON.  JSON is good for reading transfer and copy.",
      {
        acct:
          "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  " +
          "The '.' can also be used to reference the current account defined by the API key.",
        templateFile:
          "one ore more template files containing the template information to load into the database.   Can be JSON or YAML (*.YML | *.YAML) file formats.  See template examples for specifying these files.",
      }
    )
    .arguments("<acct> [templateFile...]")
    .action(function (acct, templateFile) {
      //------------------------------------------------------------------------
      // Command Implementation
      //------------------------------------------------------------------------

      for (const fname of templateFile) {
        var templates = acctools.loadSpecification(fname);
        if (!Array.isArray(templates)) {
          templates = [templates];
        }

        if (program.verbose) {
          console.info("Adding template: " + fname);
          console.info(JSON.stringify(templates, null, 2));
        }
        var inputspec = {
          acct: acct,
          templates: templates,
        };

        var req = acctools.postApiRequest(
          program,
          "/template/add",
          inputspec,
          true
        );

        request(req, (error, response, body) => {
          var resp = acctools.parseResponseBody(error, response, body);
          acctools.publishOutput(program, resp);
        });
      }
    });

  acctools.addCommonOptions(program, true);
  program.parse(process.argv);
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
