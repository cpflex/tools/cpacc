#! /user/bin/env node
/*
 *  Name:  cacc-account-services-info.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");
var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description(
      "Retrieves the detailed information for the specified service."
    )
    .arguments(
      "<acct>",
      "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key."
    )
    .arguments("[typeuri]", "Service typeuri")
    .arguments("[name]", "Service name")
    .action(function (acct, typeuri, name) {
      _acct = acct;
      _typeuri = typeuri;
      _name = name;
    });
  acctools.addCommonOptions(program);
  program.parse(process.argv);

  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------

  //Load input spec if defined.
  var inputspec = {
    acct: _acct,
    services: [],
  };

  if (_typeuri !== undefined && _name != undefined) {
    inputspec.services = [
      {
        typeuri: _typeuri,
        name: _name,
      },
    ];
  }

  var req = acctools.postApiRequest(
    program,
    "/account/services/info",
    inputspec
  );

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    acctools.publishOutput(program, resp);
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
