#! /user/bin/env node
/*
 *  Name:  cpacc-account-purge.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");
var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------
  program
    .description("Purges account from the database.  This is a secured API.")
    .option(
      "-f, --force",
      "Forces purging of an account even if not deleted first..",
      false
    )
    .arguments("<acct>", "account path or UUID")
    .action(function (acct) {
      _acct = acct;
    });
  acctools.addCommonOptions(program, true);
  program.parse(process.argv);

  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------
  //Load input spec if defined.
  var inputspec = {
    acct: _acct,
    force: program.force,
  };

  var req = acctools.postApiRequest(program, "/account/purge", inputspec, true);

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    acctools.publishOutput(program, resp);
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
