#! /user/bin/env node
/*
 *  Name:  cacc-account-services-purge.js
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2020 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
var request = require("request");
const util = require("util");
const acctools = require("./tools");
const { program } = require("commander");
var fs = require("fs");

try {
  //------------------------------------------------------------------------
  // Command arguments definition and setup.
  //------------------------------------------------------------------------

  program
    .description(
      "**REQUIRES ACCESS KEY**\r\nPurges the specified service from the account.  If no specific service is specified all services are purged."
    )
    .option(
      "-s, --spec <specfile>",
      "one or more services (specify typeuri and name) to delete specification file can be JSON (.json) or YAML (.yaml,.yml)."
    )
    .arguments(
      "<acct>",
      "The account identifier, can be an path or UUID.  Paths may be absolute (beginning with '//') or relative to the account associated with the API key.  The '.' can also be used to reference the current account defined by the API key."
    )
    .arguments("[typeuri]", "Service typeuri")
    .arguments("[name]", "Service name")
    .action(function (acct, typeuri, name) {
      _acct = acct;
      _typeuri = typeuri;
      _name = name;
    });
  acctools.addCommonOptions(program, true);
  program.parse(process.argv);

  //------------------------------------------------------------------------
  // Command Implementation
  //------------------------------------------------------------------------

  var specs = [];

  //Merge input file if defined.
  if (!(program.spec === undefined)) {
    var obj = acctools.loadSpecification(program.spec);
    if (Array.isArray(obj)) specs = specs.concat(obj);
    else specs.push(obj);
  }

  //Add command line spec if defined.
  if (_typeuri !== undefined && _name != undefined) {
    specs.push({
      typeuri: _typeuri,
      name: _name,
    });
  }

  //Load input spec if defined.
  var inputspec = {
    acct: _acct,
    services: specs,
  };

  if (specs.length == 0) throw "no services specified to purge.";

  var req = acctools.postApiRequest(
    program,
    "/account/services/purge",
    inputspec,
    true
  );

  request(req, (error, response, body) => {
    var resp = acctools.parseResponseBody(error, response, body);
    acctools.publishOutput(program, resp);
  });
} catch (e) {
  console.error(e.message);
  process.exit(-1);
}
